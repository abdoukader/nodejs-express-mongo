const student = require('../models/Student.js')

const getStudents = ((req, res) => {
    Student.find({})
        .then(result => res.status(200).json({ result }))
        .catch(error => res.status(500).json({msg: error}))
})

const getStudent = ((req, res) => {
    Student.findOne({ _id: req.params.studentID })
        .then(result => res.status(200).json({ result }))
        .catch(() => res.status(404).json({msg: 'Student not found'}))
})

const createStudent = ((req, res) => {
    Student.create(req.body)
        .then(result => res.status(200).json({ result }))
        .catch((error) => res.status(500).json({msg:  error }))
})

const updateStudent = ((req, res) => {
    Student.findOneAndUpdate({ _id: req.params.studentID }, req.body, { new: true, runValidators: true })
        .then(result => res.status(200).json({ result }))
        .catch((error) => res.status(404).json({msg: 'Student not found' }))
})

const deleteStudent = ((req, res) => {
    Student.findOneAndDelete({ _id: req.params.studentID })
        .then(result => res.status(200).json({ result }))
        .catch((error) => res.status(404).json({msg: 'Student not found' }))
})

module.exports = {
    getStudents,
    getStudent,
    createStudent,
    updateStudent,
    deleteStudent
}