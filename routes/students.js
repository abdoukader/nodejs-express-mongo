const express = require('express')
const router = express.Router()

const  {
    getStudents,
    getStudent,
    createStudent,
    updateStudent,
    deleteStudent
} = require('../controllers/students.js')

router.get('/', getStudents)

router.get('/:studentID', getStudent)

router.post('/', createStudent)

router.put('/:studentID', updateStudent)

router.delete('/:studentID', deleteStudent)

module.exports = router
